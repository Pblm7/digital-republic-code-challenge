const button = document.querySelector('#send');

button.addEventListener('click', function(e){

    e.preventDefault();

    // Tamanho da janela, porta, lata de tinta

    const tamanhosDeLata = [0.5, 2.5, 3.6, 18]; 
    const janela = 2.4;
    const porta = 1.52;
    const lataDeTinta = 5;

    // Área da parede 1

    const largura = document.querySelector("#larg1").value;
    const altura = document.querySelector("#alt1").value;
    const janelas = document.querySelector('#janela1').value;
    const portas = document.querySelector('#porta1').value;
    

    // Área da parede 2

    const largura2 = document.querySelector("#larg2").value;
    const altura2 = document.querySelector("#alt2").value;
    const janelas2 = document.querySelector('#janela2').value;
    const portas2 = document.querySelector('#porta2').value;
    
    // Área da parede 3

    const largura3 = document.querySelector("#larg3").value;
    const altura3 = document.querySelector("#alt3").value;
    const janelas3 = document.querySelector('#janela3').value;
    const portas3 = document.querySelector('#porta3').value;
    

    // Área da parede 4

    const largura4 = document.querySelector("#larg4").value;
    const altura4 = document.querySelector("#alt4").value;
    const janelas4 = document.querySelector('#janela4').value;
    const portas4 = document.querySelector('#porta4').value;

    // Fazem os calculos de areas das paredes.
    
    const area1 = altura * largura;
    const area2 = altura2 * largura2;
    const area3 = altura3 * largura3;
    const area4 = altura4 * largura4;

    // Toda área 

    const areaTotal = area1 + area2 + area3 + area4;
    const litroDeTinta = areaTotal / lataDeTinta;
    
    // Janelas total 

    const janela1 = janelas * janela;
    const janela2 = janelas2 * janela;
    const janela3 = janelas3 * janela;
    const janela4 = janelas4 * janela; 
    
    const areaTotalJanela = janela1 + janela2 + janela3 + janela4;   
    
    // Portas total 

    const porta1 = portas * porta;
    const porta2 = portas2 * porta;
    const porta3 = portas3 * porta; 
    const porta4 = portas4 * porta; 

    const areaTotalPorta = porta1 + porta2 + porta3 + porta4;
    
    // Área janela e portas 
    
    const areaJanPor = (areaTotalJanela + areaTotalPorta);

    // Área da parede com porta e janelas 

    const areaComPortaEJanelas =  areaTotal - areaJanPor;

    // Calculo de uso de tinta com janelas e portas 

    const usoDeTintaComJanEPort = areaComPortaEJanelas / lataDeTinta;
    const usoDeTintaComJanEPortAtt = (usoDeTintaComJanEPort.toFixed(2));

        console.log(usoDeTintaComJanEPort);

     // Uso de latas de tintas 

        let lataDeZeroPCinco = Math.ceil((usoDeTintaComJanEPortAtt / tamanhosDeLata[0]).toFixed(2));
        let lataDeDoisEMeio = Math.ceil((usoDeTintaComJanEPortAtt / tamanhosDeLata[1]).toFixed(2));
        let lataDeTresESeis = Math.ceil((usoDeTintaComJanEPortAtt / tamanhosDeLata[2]).toFixed(2));
        let lataDeDezoito = Math.ceil((usoDeTintaComJanEPortAtt / tamanhosDeLata[3]).toFixed(2));

    // Chamada das funções

    let camposPreenchidos = validaCamposPreenchidos(altura, altura2, altura3, altura4, largura, largura2, largura3, largura4);
    if(camposPreenchidos == false){
        alert('Preencha as informações.')
        return;
    }
    let validacaoMaster = validaMinimoMaximoDaParede(area1, area2, area3, area4);
    if(validacaoMaster == false){
        alert('A parede pode ter no mínimo 1M² e no máximo 15M².')
        document.getElementById('div-5').innerHTML = '';
        return;
    }
    areaMaximaPortaJanela(areaJanPor, areaComPortaEJanelas);
    mostrarAreaTotal(areaTotal);
    mostrarAreaValorDeLitrosDeTinta(usoDeTintaComJanEPortAtt);
    areaRealASerPintada(areaComPortaEJanelas);
    validaAlturaPorta(altura, porta, altura2, porta2, altura3, porta3, altura4, porta4);
    mostrarOpcoesDeLataAoUsuario();
    mostrarMelhorLataAoUsuario(lataDeDezoito);
    mostrarMelhorLataAoUsuario2(lataDeTresESeis);
    mostrarMelhorLataAoUsuario3(lataDeDoisEMeio);
    mostrarMelhorLataAoUsuario4(lataDeZeroPCinco);
});
