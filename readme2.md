O objetivo deste projeto é calcular o número de latas de tinta que vai ser utilizado para pintar um comôdo. 

The objective of this project it's calculate the number of paint cans that will be used to paint a room.

Como baixar: 

1- Baixe os códigos pelo GITLAB 
2- Abra a pasta de arquivos dos códigos
3- Clique no arquivo 'index.html'

How to download: 

1- Download the codes on the GITLAB
2- Open the code files folder
3- Click on the archive 'index.html'

O projeto funciona da seguinte maneira, tem que ser informado a altura e largura das paredes
e janelas e portas do cômodo, e com essas informações o projeto irá calcular a área total, 
área a ser pintada, litros de tinta a ser usado, e as opções de lata de tinta.  

The project works as follows, the height and width of the walls must be informed
and windows and doors of the room, and with this information the project will calculate 
the total area, area to be painted, liters of paint to be used, and paint can options.