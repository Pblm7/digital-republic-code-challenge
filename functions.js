 // Function de minimo e máximo de metro quadrados da parede.     
    
    

 function validaMinimoMaximoDaParede(area1, area2, area3, area4){
    return (area1 >= 1 && area1 <= 15) || 
    (area2 >= 1 && area2 <= 15) || 
    (area3 >= 1 && area3 <= 15) || 
    (area4 >= 1 && area4 <= 15)
};  


function validaCamposPreenchidos(altura, altura2, altura3, altura4, largura, largura2, largura3, largura4){
    return altura != '' && 
    altura2 != '' && 
    altura3 != '' && 
    altura4 != '' &&
    largura != '' && 
    largura2 != '' && 
    largura3 != '' && 
    largura4 != '';
};

function areaMaximaPortaJanela(areaJanPor, areaComPortaEJanelas){
if(areaJanPor > areaComPortaEJanelas / 2){
    alert('A área total de janelas e portas não podem ser maior que 50% de toda área do cômodo.')
}	
};

// Functions de altura da porta. 

function validaAlturaPorta(altura, porta, altura2, porta2, altura3, porta3, altura4, porta4){
if(porta > 0 &&  altura >= 1 && altura <= 2.20 || 
    porta2 > 0 && altura2 >= 1 && altura2 <= 2.20 || 
    porta3 > 0 && altura3 >= 1 && altura3 <= 2.20 || 
    porta4 > 0 && altura4 >= 1 && altura4 <= 2.20) {
    alert('A altura da parede tem que ser maior que 2.20 metros.')
}
};

// Functions para adicionar a informações a página. 

function mostrarAreaTotal(areaTotal){
document.getElementById('infos').innerHTML = `Área total: ${areaTotal}M².`
};

function areaRealASerPintada(areaComPortaEJanelas){
document.getElementById('infos-2').innerHTML = `Área a ser pintada: ${areaComPortaEJanelas}M².`
};

function mostrarAreaValorDeLitrosDeTinta(usoDeTintaComJanEPortAtt){
document.getElementById('infos-3').innerHTML = `Litros de tinta: ${usoDeTintaComJanEPortAtt}L.`
};

function mostrarOpcoesDeLataAoUsuario(){
document.getElementById('infos-4').innerHTML = 'Opções: '
}

function mostrarMelhorLataAoUsuario(lataDeDezoito){
document.getElementById('infos-5').innerHTML = `${lataDeDezoito} Lata(s) de 18L.`
};

function mostrarMelhorLataAoUsuario2(lataDeTresESeis){
document.getElementById('infos-6').innerHTML = `${lataDeTresESeis} Lata(s) de 3,6L.`
};

function mostrarMelhorLataAoUsuario3(lataDeDoisEMeio){
document.getElementById('infos-7').innerHTML = `${lataDeDoisEMeio} Lata(s) de 2,5L.`
};

function mostrarMelhorLataAoUsuario4(lataDeZeroPCinco){
document.getElementById('infos-8').innerHTML = `${lataDeZeroPCinco} Lata(s) de 0,5L.`
};

function evitarCalculosDesnecessesarios(camposPreenchidos){
if(camposPreenchidos == true){
    return false; 
}
};
